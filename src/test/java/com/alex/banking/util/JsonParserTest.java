package com.alex.banking.util;

import com.alex.banking.dto.AccountMoneyTransferDto;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JsonParserTest {

    @InjectMocks
    private JsonParser jsonParser;
    @Mock
    private ObjectMapper objectMapper;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldParseJsonToAccountMoneyTransferDto() throws IOException {
        //GIVEN
        String json = "{\"accountFrom\" : 1, \"accountTo\" : 2, \"amount\" : 500}";
        AccountMoneyTransferDto expected = new AccountMoneyTransferDto();
        expected.setAccountFrom(1L);
        expected.setAccountTo(2);
        expected.setAmount(500);
        when(objectMapper.readValue(json, AccountMoneyTransferDto.class))
                .thenReturn(expected);
        //WHEN
        AccountMoneyTransferDto actual = jsonParser.parseJson(AccountMoneyTransferDto.class, json);
        //THEN
        assertThat(actual, is(not(nullValue())));
        assertThat(actual.getAccountFrom(), is(expected.getAccountFrom()));
        assertThat(actual.getAccountTo(), is(expected.getAccountTo()));
        assertThat(actual.getAmount(), is(expected.getAmount()));
    }

    @Test
    public void shouldCatchIoExceptionAndThrowIlligalArgumentException() throws IOException {
        //GIVEN
        expectedException.expect(IllegalArgumentException.class);
        String wrongJson = "lmao";
        when(objectMapper.readValue(wrongJson, AccountMoneyTransferDto.class))
                .thenThrow(new IOException());
        //WHEN
        jsonParser.parseJson(AccountMoneyTransferDto.class, wrongJson);
        //THEN exception should be thrown
    }

    @Test
    public void shouldParseObjectToJson() throws JsonProcessingException {
        //GIVEN
        String expectedJson = "{\"accountFrom\" : 1, \"accountTo\" : 2, \"amount\" : 500}";
        AccountMoneyTransferDto dto = new AccountMoneyTransferDto();
        dto.setAccountFrom(1L);
        dto.setAccountTo(2);
        dto.setAmount(500);
        when(objectMapper.writeValueAsString(dto))
                .thenReturn(expectedJson);
        //WHEN
        String actualJson = jsonParser.toJson(dto);
        //THEN
        assertThat(actualJson, is(equalTo(expectedJson)));
    }

    @Test
    public void shouldCatchJsonProcessingExceptionAndThrowIllegalArgumentExcepiton() throws JsonProcessingException {
        //GIVEN
        when(objectMapper.writeValueAsString(Mockito.any(AccountMoneyTransferDto.class)))
                .thenThrow(new JsonGenerationException(new NullPointerException()));
        expectedException.expect(IllegalArgumentException.class);
        //WHEN
        jsonParser.toJson(null);
        //THEN exception should be thrown
    }
}