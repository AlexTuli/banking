package com.alex.banking.service;

import com.alex.banking.config.BankingModule;
import com.alex.banking.exception.NotEnoughMoneyException;
import com.alex.banking.exception.ServiceException;
import com.alex.banking.model.Account;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AccountServiceIntegrationTest {

    private static SessionFactory sessionFactory;
    private static AccountService accountService;
    private Session session;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void setUP() {
        Injector injector = Guice.createInjector(new BankingModule());
        sessionFactory = injector.getInstance(SessionFactory.class);
        accountService = injector.getInstance(AccountService.class);
    }

    @Before
    public void setUp() throws Exception {
        session = sessionFactory.openSession();
    }

    @After
    public void tearDown() throws Exception {
        session.close();
    }

    @Test
    public void shouldCreateAccountAndAddMoney() {
        //WHEN
        long account = accountService.createAccount();
        accountService.depositMoney(account, 600);
        //THEN
        Account persisted = session.find(Account.class, account);
        assertThat(persisted.getAmount(), is(equalTo(600L)));
    }

    @Test
    public void shouldTransferMoney() {
        //GIVEN
        long first = accountService.createAccount();
        accountService.depositMoney(first, 100L);
        long second = accountService.createAccount();
        accountService.depositMoney(second, 500L);
        //WHEN
        accountService.transferMoney(second, first, 300);
        //THEN
        assertThat(accountService.getAccount(first).getAmount(), is(equalTo((100L + 300L))));
        assertThat(accountService.getAccount(second).getAmount(), is(equalTo((500L - 300L))));
    }

    @Test
    public void shouldThrowServiceExceptionWhenWhenAmountIsNegative() {
        //GIVEN
        expectedException.expect(ServiceException.class);
        //WHEN
        accountService.depositMoney(1, -5L);
        //THEN exception should be thrown
    }

    @Test
    public void shouldThrowDaoExceptionWhenIdIsNotExists() {
        //GIVEN
        expectedException.expect(ServiceException.class);
        //WHEN
        accountService.depositMoney(-5L, 500);
        //THEN exception should be thrown
    }

    @Test
    public void shouldThrowException() {
        //GIVEN
        expectedException.expect(NotEnoughMoneyException.class);
        Account first = createAccount(50);
        Account second = createAccount(10);
        //WHEN
        accountService.transferMoney(second.getId(), first.getId(), 75);
        //THEN exception should be throw
    }

    @Test
    public void shouldTransferMoneyInMultipleThreads() throws InterruptedException {
        //GIVEN
        Account first = createAccount(500);
        Account second = createAccount(1000);
        Account third = createAccount(750);
        long expectedTotal = first.getAmount() + second.getAmount() + third.getAmount();

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        //WHEN
        for (int i = 0; i < 3; i++) {
            executorService.submit(() -> {
                //second -> first 100
                accountService.transferMoney(second.getId(), first.getId(), 100L);
                //third -> second 50
                accountService.transferMoney(third.getId(), second.getId(), 50L);
                //first -> third 25
                accountService.transferMoney(first.getId(), third.getId(), 25L);
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        //THEN
        Account firstUpdated = accountService.getAccount(first.getId());
        Account secondUpdated = accountService.getAccount(second.getId());
        Account thirdUpdated = accountService.getAccount(third.getId());

        long actualTotal = firstUpdated.getAmount() + secondUpdated.getAmount() + thirdUpdated.getAmount();
        assertThat(actualTotal, is(equalTo(expectedTotal)));

        long secondExpected = 1000L - (100L * 3L) + (50L * 3);
        assertThat(secondUpdated.getAmount(), is(equalTo(secondExpected)));

        long firstExpected = 500L + (100L * 3L) - (25L * 3);
        assertThat(firstUpdated.getAmount(), is(equalTo(firstExpected)));

        long thirdExpected = 750L - (50L * 3L) + (25L * 3L);
        assertThat(thirdUpdated.getAmount(), is(equalTo(thirdExpected)));
    }

    private Account createAccount(long initialAmount) {
        long account = accountService.createAccount();
        accountService.depositMoney(account, initialAmount);
        return accountService.getAccount(account);
    }
}