package com.alex.banking.controller;

import com.alex.banking.Application;
import com.alex.banking.repository.AccountRepository;
import com.alex.banking.dto.AccountMoneyTransferDto;
import com.alex.banking.model.Account;
import com.alex.banking.util.JsonParser;
import com.despegar.http.client.*;
import com.despegar.sparkjava.test.SparkServer;
import com.google.inject.Injector;
import org.junit.ClassRule;
import org.junit.Test;
import spark.servlet.SparkApplication;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class AccountControllerIntegrationTest {

    private static AccountRepository repository;
    private static JsonParser jsonParser;


    public static class BankingTestApplication implements SparkApplication {
        @Override
        public void init() {
            Injector injector = Application.startContainer();
            repository = injector.getInstance(AccountRepository.class);
            jsonParser = injector.getInstance(JsonParser.class);
        }
    }

    @ClassRule
    public static SparkServer<BankingTestApplication> testServer = new SparkServer<>(BankingTestApplication.class, 4556);

    @Test
    public void shouldCreateAccount() throws HttpClientException {
        //GIVEN
        PostMethod create = testServer.post(AccountController.ACCOUNT_PATH + AccountController.CREATE, "", false);
        //WHEN
        HttpResponse response = testServer.execute(create);
        //THEN
        assertThat(response.code(), is(200));
        assertThat(response.body().length, is(not(equalTo(0))));
    }

    @Test
    public void shouldReturn400WhenDepositWithStringId() throws HttpClientException {
        //GIVEN
        String expectedErrorMessage = "Please type correct Amount or AccountId";
        PutMethod deposit = testServer.put(String.format("/account/%s/deposit/%d", "wrongId", 500), "", false);
        //WHEN
        HttpResponse response = testServer.execute(deposit);
        //THEN
        assertThat(response.code(), is(equalTo(400)));
        assertThat(new String(response.body()), is(equalTo(expectedErrorMessage)));
    }

    @Test
    public void shouldCreateAccountAndDepositMoney() throws HttpClientException {
        //GIVEN
        long accountId = repository.create(new Account());

        long amount = 500L;
        PutMethod deposit = testServer.put(getDepositUrl(accountId, amount), "", false);
        //WHEN
        HttpResponse response = testServer.execute(deposit);
        //THEN
        assertThat(response.code(), is(200));
        String body = new String(response.body());
        assertThat(body, is("SUCCESS"));

        Account account = repository.findById(accountId);
        assertThat(account.getAmount(), is(equalTo(amount)));
    }

    @Test
    public void shouldRetrieveAccount() throws HttpClientException {
        //GIVEN
        long accountId = repository.create(new Account());

        GetMethod getMethod = testServer.get(String.format("/account/%d", accountId), false);
        //WHEN
        HttpResponse response = testServer.execute(getMethod);
        //THEN
        assertThat(response.code(), is(200));
        Account account = jsonParser.parseJson(Account.class, new String(response.body()));
        assertThat(account.getId(), is(equalTo(accountId)));
    }

    @Test
    public void shouldReturn400WhenTransferWithNonExistingAccount() throws HttpClientException {
        //GIVEN
        String expectedErrorMessage = "Account does not exist";

        AccountMoneyTransferDto dto = new AccountMoneyTransferDto();
        dto.setAccountTo(-5);
        dto.setAmount(500);

        PutMethod trasfer = testServer.put(AccountController.ACCOUNT_PATH + AccountController.TRANSFER, jsonParser.toJson(dto), false);
        //WHEN
        HttpResponse response = testServer.execute(trasfer);
        //THEN
        assertThat(response.code(), is(equalTo(400)));
    }

    @Test
    public void shouldTransferMoney() throws HttpClientException {
        //GIVEN
        long first = repository.create(new Account());
        long second = repository.create(new Account());

        repository.depositMoney(first, 500);

        AccountMoneyTransferDto dto = new AccountMoneyTransferDto();
        dto.setAccountFrom(first);
        dto.setAccountTo(second);
        dto.setAmount(100);

        PutMethod put = testServer.put(AccountController.ACCOUNT_PATH + AccountController.TRANSFER, jsonParser.toJson(dto), false);
        //WHEN
        HttpResponse response = testServer.execute(put);
        //THEN
        assertThat(response.code(), is(200));
        assertThat(repository.findById(first).getAmount(), is(equalTo(400L)));
        assertThat(repository.findById(second).getAmount(), is(equalTo(100L)));
    }

    private String getDepositUrl(long accountId, long amount) {
        return String.format("/account/%d/deposit/%d", accountId, amount);
    }
}