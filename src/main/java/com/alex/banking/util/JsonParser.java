package com.alex.banking.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsonParser {

    private static final Logger log = LoggerFactory.getLogger(JsonParser.class);

    @Inject
    private ObjectMapper mapper;

    public <T> T parseJson(Class<T> type, String json) {
        try {
            return mapper.readValue(json, type);
        } catch (IOException e) {
            log.error("Can't parse JSON", e);
            throw new IllegalArgumentException(e);
        }
    }

    public String toJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("Can't serialize object", e);
            throw new IllegalArgumentException(e);
        }
    }
}
