package com.alex.banking.repository;

import com.alex.banking.exception.NotEnoughMoneyException;
import com.alex.banking.exception.RepositoryException;
import com.alex.banking.model.Account;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.LockModeType;
import java.util.Arrays;
import java.util.List;

public class AccountRepository extends AbstractRepository<Account, Long> {

    private static final Logger log = LoggerFactory.getLogger(AccountRepository.class);

    public void depositMoney(long accountId, long amount) {
        execute(s -> {
            Account account = s.find(Account.class, accountId, LockModeType.PESSIMISTIC_WRITE);
            if (account == null) {
                throw new RepositoryException("Account does not exist");
            }
            account.setAmount(account.getAmount() + amount);
            s.saveOrUpdate(account);
            return Void.TYPE;
        });
    }

    public void transferMoney(long accountFrom, long accountTo, long amount) {
        execute(s -> {
            List<Account> accounts = getAccounts(accountFrom, accountTo, s);

            if (accounts.size() != 2) {
                throw new RepositoryException("Account(s) does not exist");
            }
            Account from = getAccountById(accountFrom, accounts);
            Account to = getAccountById(accountTo, accounts);

            verifyAccountHasEnoughAmount(amount, from);
            from.setAmount(from.getAmount() - amount);
            to.setAmount(to.getAmount() + amount);

            s.saveOrUpdate(from);
            s.saveOrUpdate(to);

            return Void.TYPE;
        });
    }

    private void verifyAccountHasEnoughAmount(long amount, Account from) {
        if (from.getAmount() < amount) {
            log.error("Account [id:{}] has less than [{}]", from.getAmount(), amount);
            throw new NotEnoughMoneyException(String.format("Account has less than [%d]", amount));
        }
    }


    private List<Account> getAccounts(long accountFrom, long accountTo, Session s) {
        return s.createNamedQuery("ACCOUNT.findByIds", Account.class)
                .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                .setParameter("ids", Arrays.asList(accountFrom, accountTo))
                .getResultList();
    }

    private Account getAccountById(long id, List<Account> accs) {
        return accs.stream().filter(e -> e.getId() == id).findFirst().orElseThrow(IllegalStateException::new);
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
