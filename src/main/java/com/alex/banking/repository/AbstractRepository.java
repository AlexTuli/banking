package com.alex.banking.repository;

import com.google.inject.Inject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.function.Function;

public abstract class AbstractRepository<T, E extends Serializable> {

    @Inject
    private SessionFactory factory;

    @SuppressWarnings("unchecked")
    public E create(T t) {
        return execute(s -> (E) s.save(t));
    }

    public T findById(E e) {
        return execute(s -> s.get(getRepositoryType(), e));
    }

    @SuppressWarnings("unchecked")
    private Class<T> getRepositoryType() {
        return (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected <R> R execute(Function<Session, R> command) {
        try (Session s = factory.openSession()) {
            Transaction tx = null;
            try {
                tx = s.beginTransaction();
                R result = command.apply(s);
                tx.commit();
                return result;
            } catch (RuntimeException e) {
                getLog().error("Can't execute request", e);
                if (tx != null) {
                    tx.rollback();
                }
                throw e;
            }
        }
    }

    protected abstract Logger getLog();
}
