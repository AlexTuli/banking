package com.alex.banking.repository;

import com.alex.banking.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserRepository extends AbstractRepository<User, Long> {

    private static final Logger log = LoggerFactory.getLogger(UserRepository.class);


    @Override
    protected Logger getLog() {
        return log;
    }
}
