package com.alex.banking;

import com.alex.banking.config.BankingModule;
import com.alex.banking.config.factory.JettyConfigurationFactory;
import com.alex.banking.controller.AccountController;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.jetty.server.RequestLog;
import org.slf4j.LoggerFactory;
import spark.embeddedserver.EmbeddedServers;
import spark.embeddedserver.jetty.EmbeddedJettyFactory;

import static spark.Spark.*;
import static spark.embeddedserver.EmbeddedServers.Identifiers.JETTY;

public class Application {

    public static void main(String[] args) {
        //JETTY Start
        startJettyServer();
        startContainer();

    }

    public static Injector startContainer() {
        //GUICE
        Injector injector = Guice.createInjector(new BankingModule());
        AccountController accountController = injector.getInstance(AccountController.class);

        //Spark config
        path(AccountController.ACCOUNT_PATH, () -> {
            post(AccountController.CREATE, accountController.CREATE_ACCOUNT);
            put(AccountController.DEPOSIT, accountController.DEPOSIT_MONEY);
            get(AccountController.GET, accountController.GET_ACCOUNT);
            put(AccountController.TRANSFER, accountController.TRANSFER_MONEY);
        });


        return injector;
    }

    private static void startJettyServer() {
        RequestLog requestLog = JettyConfigurationFactory.requestLog(LoggerFactory.getLogger(Application.class));
        EmbeddedJettyFactory embeddedJettyFactory = JettyConfigurationFactory.embeddedJettyFactory(requestLog);
        EmbeddedServers.add(JETTY, embeddedJettyFactory);
    }
}
