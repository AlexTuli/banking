package com.alex.banking.controller;

import com.alex.banking.dto.AccountMoneyTransferDto;
import com.alex.banking.exception.ServiceException;
import com.alex.banking.model.Account;
import com.alex.banking.service.AccountService;
import com.alex.banking.util.JsonParser;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Route;

import java.util.Optional;

public class AccountController {

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    public static final String ACCOUNT_PATH = "/account";
    public static final String CREATE = "/create";
    public static final String GET = "/:accountId";
    public static final String DEPOSIT = GET + "/deposit/:amount";
    public static final String TRANSFER = "/transfer";

    private static final String ACCOUNT_ID = "accountId";
    private static final String AMOUNT = "amount";


    @Inject
    private AccountService accountService;
    @Inject
    private JsonParser jsonParser;

    public final Route CREATE_ACCOUNT = (request, response) -> accountService.createAccount();

    public final Route GET_ACCOUNT = (request, response) -> {
        Optional<Long> accountId = extractLong(request.params(ACCOUNT_ID));
        if (accountId.isPresent()) {
            Account account = accountService.getAccount(accountId.get());
            if (account != null) {
                return jsonParser.toJson(account);
            } else {
                response.status(400);
                return "Account does not exist";
            }
        } else {
            response.status(400);
            return "Please fill accountId";
        }
    };

    public final Route DEPOSIT_MONEY = (request, response) -> {
        try {
            Optional<Long> accountId = extractLong(request.params(ACCOUNT_ID));
            Optional<Long> amount = extractLong(request.params(AMOUNT));
            if (accountId.isPresent() && amount.isPresent()) {
                accountService.depositMoney(accountId.get(), amount.get());
                return "SUCCESS";
            } else {
                log.error("Amount or Account ID have wrong format");
                response.status(400);
                return "Please type correct Amount or AccountId";
            }
        } catch (ServiceException e) {
            log.error("Incorrect money amount", e);
            response.status(400);
            return e.getMessage();
        }
    };

    public final Route TRANSFER_MONEY = (request, response) -> {
        AccountMoneyTransferDto dto = jsonParser.parseJson(AccountMoneyTransferDto.class, request.body());
        try {
            accountService.transferMoney(dto.getAccountFrom(), dto.getAccountTo(), dto.getAmount());
            return "SUCCESS";
        } catch (ServiceException e) {
            log.error("Can't transfer money", e);
            response.status(400);
            return e.getMessage();
        }
    };

    private Optional<Long> extractLong(String param) {
        try {
            return Optional.of(Long.parseLong(param));
        } catch (NumberFormatException | NullPointerException e) {
            log.error("Wrong number format", e);
            return Optional.empty();
        }
    }
}
