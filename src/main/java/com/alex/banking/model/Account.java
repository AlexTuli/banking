package com.alex.banking.model;

import javax.persistence.*;

@Entity
@Table
@NamedQueries({
        @NamedQuery(name = "ACCOUNT.findByIds", query = "SELECT a FROM Account a WHERE a.id IN (:ids)")
})
public class Account {

    @Id
    @GeneratedValue
    private Long id;
    private long amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Amount " + this.amount;
    }
}
