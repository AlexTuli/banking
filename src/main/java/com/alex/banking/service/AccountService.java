package com.alex.banking.service;

import com.alex.banking.repository.AccountRepository;
import com.alex.banking.exception.RepositoryException;
import com.alex.banking.exception.ServiceException;
import com.alex.banking.model.Account;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountService {

    private static final Logger log = LoggerFactory.getLogger(AccountService.class);

    @Inject
    private AccountRepository accountRepository;

    public long createAccount() {
        return accountRepository.create(new Account());
    }

    public Account getAccount(long accountId) {
        return accountRepository.findById(accountId);
    }

    public void depositMoney(long accountId, long amount) {
        try {
            accountRepository.depositMoney(accountId, requirePositiveAmount(amount, "You can't deposit zero or negative amount of money"));
            log.info("Account [{}] has been replenished", accountId);
        } catch (RepositoryException e) {
            log.error("Account does not exist", e);
            throw new ServiceException("Account does not exist");
        }
    }

    public void transferMoney(long from, long to, long amount) {
        try {
            accountRepository.transferMoney(from, to, requirePositiveAmount(amount, "You can't transfer zero or negative amount of money"));
            log.trace("Money have been transferred from [{}] to [{}]", from, to);
        } catch (RepositoryException e) {
            log.error("Account does not exist", e);
            throw new ServiceException(e);
        }
    }

    private long requirePositiveAmount(long amount, String errorMessage) {
        if (amount < 1) {
            log.warn("Non positive amount");
            throw new ServiceException(errorMessage);
        }
        return amount;
    }

}
