package com.alex.banking.exception;

public class NotEnoughMoneyException extends ServiceException {

    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
