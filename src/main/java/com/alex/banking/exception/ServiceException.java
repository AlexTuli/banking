package com.alex.banking.exception;

public class ServiceException extends BusinessException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
