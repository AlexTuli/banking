package com.alex.banking.exception;

public class RepositoryException extends BusinessException {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
