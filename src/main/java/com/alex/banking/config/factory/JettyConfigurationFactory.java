package com.alex.banking.config.factory;

import com.alex.banking.config.factory.server.JettyServerConstructor;
import com.alex.banking.config.factory.server.RequestLogFactory;
import org.eclipse.jetty.server.RequestLog;
import org.slf4j.Logger;
import spark.embeddedserver.jetty.EmbeddedJettyFactory;

public class JettyConfigurationFactory {

    public static EmbeddedJettyFactory embeddedJettyFactory(RequestLog requestLog) {
        return JettyServerConstructor.create(requestLog);
    }

    public static RequestLog requestLog(Logger logger) {
        return RequestLogFactory.create(logger);
    }
}
