package com.alex.banking.config.factory.server;

import org.eclipse.jetty.server.AbstractNCSARequestLog;
import org.eclipse.jetty.server.RequestLog;
import org.slf4j.Logger;

import java.io.IOException;

public class RequestLogFactory {

    public static RequestLog create(Logger logger) {
        return new AbstractNCSARequestLog() {
            @Override
            protected boolean isEnabled() {
                return true;
            }

            @Override
            public void write(String s) throws IOException {
                logger.info(s);
            }
        };
    }
}
