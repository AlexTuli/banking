package com.alex.banking.config;

import com.alex.banking.config.factory.JpaFactory;
import com.google.inject.AbstractModule;
import org.hibernate.SessionFactory;

public class BankingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SessionFactory.class)
                .toInstance(JpaFactory.getSessionFactory());
    }
}
