# banking

* Run **mvn clean install** (to run Integration Tests enable _-P integration-tests_)
* Run **java -jar target/banking-1.0-SNAPSHOT-jar-with-dependencies.jar**
* Open postman. Call **POST** http://localhost:4567/accout/create to create Account
* You may deposit money to Account by **PUT** http://localhost:4567/accout/{id}/deposit/amount
* You may transfer money from one Account to another by **PUT** http://localhost:4567/accout/transfer with next JSON: 
{
	"accountFrom" : 1,
	"accountTo" : 2,
	"amount" : 500
}
* You may see your Account details by **GET** http://localhost:4567/accout/{id}